package cs.mad.flashcards.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.telecom.Call
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.FlashCardDatabase
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardContainer
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.runOnIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var fcDao: FlashcardDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashCardDatabase::class.java, FlashCardDatabase.databaseName
        ).build()
        fcDao = db.fcDao()





        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard(null,"test", "test"))
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.deleteSetButton.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
        }





        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.flashcardList.adapter = FlashcardAdapter(fcDao)
        loadData()
    }



    private fun loadData(){
        runOnIO { (binding.flashcardList.adapter as FlashcardAdapter).addallItems(fcDao.getAll()) }
    }
}