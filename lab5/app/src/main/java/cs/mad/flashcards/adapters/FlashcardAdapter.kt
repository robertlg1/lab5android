package cs.mad.flashcards.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.runOnIO
import cs.mad.flashsets.entities.FlashcardSet

class FlashcardAdapter(private val fcDao: FlashcardDao) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = mutableListOf<Flashcard>()
    var index = 0

    init {
        this.dataSet.addAll(dataSet)
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question
        viewHolder.itemView.setOnClickListener {
            AlertDialog.Builder(it.context)
                    .setTitle(item.question)
                    .setMessage(item.answer)
                    .setPositiveButton("Done") { _,_ -> }
                    .setNeutralButton("Edit") { _, _ -> showEditDialog(it.context, item) }
                    .create()
                    .show()
        }
        viewHolder.itemView.setOnLongClickListener {
            showEditDialog(it.context, item)
            true
        }
    }

    private fun showEditDialog(context: Context, flashcard: Flashcard) {
        val customTitle = EditText(context)
        val customBody = EditText(context)
        customTitle.setText(flashcard.question)
        customTitle.textSize = 20 * context.resources.displayMetrics.scaledDensity
        customBody.setText(flashcard.answer)
        AlertDialog.Builder(context)
                .setCustomTitle(customTitle)
                .setView(customBody)
                .setPositiveButton("Done") { _,_ ->
                    index = dataSet.indexOf(flashcard)
                    flashcard.question = customTitle.text.toString()
                    flashcard.answer = customBody.text.toString()
                    runOnIO { fcDao.update(flashcard)}
                    notifyItemChanged(index)
                }
                .setNegativeButton("Delete") { _,_ ->
                    runOnIO { fcDao.delete(flashcard) }
                    index = dataSet.indexOf(flashcard)
                    dataSet.remove(flashcard)
                    notifyItemRemoved(index)
                }
                .create()
                .show()
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun deleteItem(it: Flashcard) {
        dataSet.remove(it)
        notifyItemInserted(dataSet.lastIndex)
    }

    fun addItem(it: Flashcard) {
        runOnIO {
            it.id = fcDao.insert(it)
        }
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }

    fun addallItems(list: List<Flashcard>) {
        list?.let {
            dataSet.addAll(it)
            notifyDataSetChanged()
        }
    }
}