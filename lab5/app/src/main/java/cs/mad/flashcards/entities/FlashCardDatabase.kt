package cs.mad.flashcards.entities

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Flashcard::class], version = 1)
abstract class FlashCardDatabase: RoomDatabase(){
    companion object{
        const val databaseName = "Flashcard_Database"
    }

    abstract fun fcDao(): FlashcardDao
}