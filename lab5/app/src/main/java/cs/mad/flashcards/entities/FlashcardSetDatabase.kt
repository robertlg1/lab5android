package cs.mad.flashcards.entities

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.flashsets.entities.FlashcardSet
import cs.mad.flashsets.entities.FlashcardSetDao

@Database(entities = [FlashcardSet::class], version = 1)
abstract class FlashcardSetDatabase: RoomDatabase(){
    companion object{
        const val setDatabaseName = "FlashcardSet_Database"
    }
    abstract fun flcsetDao(): FlashcardSetDao
}