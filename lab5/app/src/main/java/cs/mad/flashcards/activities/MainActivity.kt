package cs.mad.flashcards.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.telecom.Call
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashCardDatabase
//import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDatabase
import cs.mad.flashcards.runOnIO
import cs.mad.flashsets.entities.FlashcardSet
import cs.mad.flashsets.entities.FlashcardSetContainer
import cs.mad.flashsets.entities.FlashcardSetDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var flcsetDao: FlashcardSetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext, FlashcardSetDatabase::class.java,
            FlashcardSetDatabase.setDatabaseName).build()
        flcsetDao = db.flcsetDao()




        binding.createSetButton.setOnClickListener {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet(null,"title 11"))

            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }

        binding.flashcardSetList.adapter = FlashcardSetAdapter(flcsetDao)
        loadData()
    }

    private fun loadData(){
            runOnIO {
                (binding.flashcardSetList.adapter as FlashcardSetAdapter).insertAll(flcsetDao.getAll())
            }
    }


}