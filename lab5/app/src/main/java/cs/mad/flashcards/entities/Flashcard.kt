package cs.mad.flashcards.entities

import androidx.room.*


data class Flashcard2(
    val question: String,
    val answer: String
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard(null,"Term i", "Def i"))
            }
            return cards
        }
    }
}

data class FlashcardContainer(
    val flashcards: List<Flashcard>
)
@Entity
data class Flashcard(
    @PrimaryKey var id: Long?,
    var question: String,
    var answer: String
) {
    fun getHardcodedflashcards(): List<Flashcard> {
        val cards = mutableListOf<Flashcard>()
        for (i in 1..10) {
            cards.add(Flashcard(null,"Term $i", "Def $i"))
        }

        return cards
    }
}

@Dao
interface FlashcardDao {
    @Query("select * from flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(flc: Flashcard): Long

    @Insert
    suspend fun insertAll(flcs: List<Flashcard>)

    @Update
    suspend fun update(flc: Flashcard)

    @Delete
    suspend fun  delete(flc: Flashcard)

}