package cs.mad.flashsets.entities

import androidx.room.*
import cs.mad.flashcards.entities.Flashcard


data class  FlashcardSetContainer(
    val flashcardsets: List<FlashcardSet>
)

@Entity
data class FlashcardSet(
    @PrimaryKey var ID: Long?,
    val title: String
){
    fun getHardcodedFlashcardSets(): List<FlashcardSet> {
        val sets = mutableListOf<FlashcardSet>()
        for (i in 1..10) {
            sets.add(FlashcardSet(null, "Title $i"))
        }
        return sets
    }
}

@Dao
interface FlashcardSetDao {
    @Query("select * from flashcardset")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(flc: FlashcardSet)

    @Insert
    suspend fun insertAll(flcs: List<FlashcardSet>)

    @Update
    suspend fun update(flc: FlashcardSet)

    @Delete
    suspend fun  delete(flc: FlashcardSet)

    @Query("delete from flashcardset")
    suspend fun deleteAll()

}

